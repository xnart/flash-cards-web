import Vue from 'vue';
import Router from 'vue-router';
import Login from '../Login'
import Dashboard from "@/Dashboard";

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    }
  ]
});

export default router