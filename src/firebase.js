import firebase from "firebase/app";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyDAN51FWJ0PBU4xdiVEmFa10gq4dQ4EBu8",
  authDomain: "vocab-flashcards-faa22.firebaseapp.com",
  databaseURL: "https://vocab-flashcards-faa22.firebaseio.com",
  projectId: "vocab-flashcards-faa22",
  storageBucket: "vocab-flashcards-faa22.appspot.com",
  messagingSenderId: "133114430125",
  appId: "1:133114430125:web:deef82c5480adf55fd7c24",
  measurementId: "G-E461ZQZPX9"
};
let myApp = firebase.initializeApp(firebaseConfig);
firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL).then(r => console.log(r));

export default myApp;