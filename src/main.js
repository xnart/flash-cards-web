import Vue from 'vue'
import App from './App.vue'
import VueCropper from 'vue-cropperjs';
import 'cropperjs/dist/cropper.css';
import vuetify from './plugins/vuetify';
import router from "./routes/index";
import firebase from "./firebase";
import "firebase/auth";

Vue.component(VueCropper);

Vue.config.productionTip = false;

router.beforeEach((to, from, next) => {
  firebase.auth().onAuthStateChanged((currentUser) => {
    if (currentUser == null && to.path !== "/login") {
      if (router.currentRoute.path !== "/login") {
        next({path: "/login"});
      }
    } else {
      next();
    }
  });
});


new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
